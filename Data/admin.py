from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Folder)
admin.site.register(Image)
admin.site.register(Mod)
admin.site.register(DelAct)
admin.site.register(CreateAct)
admin.site.register(RemoveAct)
