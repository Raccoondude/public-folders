from django import forms

class UploadForm(forms.Form):
    File = forms.FileField()

class ModForm(forms.Form):
    Username = forms.CharField()
    Password = forms.CharField()
    ID = forms.IntegerField()
    Note = forms.CharField(widget=forms.Textarea)

class CreateForm(forms.Form):
    Username = forms.CharField()
    Password = forms.CharField()
    Name = forms.CharField()
    Note = forms.CharField(widget=forms.Textarea)
    
class RemoveForm(forms.Form):
    Username = forms.CharField()
    Password = forms.CharField()
    ID = forms.IntegerField()
    Note = forms.CharField(widget=forms.Textarea)

class SubmitForm(forms.Form):
    Username = forms.CharField()
    Password = forms.CharField()
