# Generated by Django 2.2.3 on 2019-07-22 17:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Data', '0008_mod_mod_ip'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mod',
            name='mod_IP',
            field=models.IntegerField(default=0),
        ),
    ]
