from django.db import models

# Create your models here.

class Folder(models.Model):
    folder_ID = models.AutoField(primary_key=True)
    folder_name = models.CharField(max_length=25)
    def __str__(self):
        return self.folder_name

class Image(models.Model):
    image_ID = models.AutoField(primary_key=True)
    image_IP = models.CharField(max_length=25)
    image_image = models.ImageField(upload_to="OwO")
    image_folder = models.ForeignKey(Folder, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.image_image)

class Mod(models.Model):
    mod_ID = models.AutoField(primary_key=True)
    mod_username = models.CharField(max_length=25)
    mod_IP = models.CharField(default='', max_length=25)
    mod_password = models.CharField(max_length=50)
    mod_is_verifyed = models.BooleanField(default=False)
    mod_can_delete = models.BooleanField(default=False)
    mod_can_create = models.BooleanField(default=False)
    mod_can_remove = models.BooleanField(default=False)
    def __str__(self):
        return self.mod_username

class DelAct(models.Model):
    act_ID = models.AutoField(primary_key=True)
    act_note = models.TextField(max_length=1000)
    act_username = models.CharField(max_length=25)
    act_post_ID = models.IntegerField()
    def __str__(self):
        return str(self.act_ID)

class CreateAct(models.Model):
    act_ID = models.AutoField(primary_key=True)
    act_folder_ID = models.IntegerField()
    act_username = models.CharField(max_length=25)
    act_note = models.TextField(max_length=1000)

class RemoveAct(models.Model):
    act_ID = models.AutoField(primary_key=True)
    act_folder_ID = models.IntegerField()
    act_username = models.CharField(max_length=25)
    act_note = models.TextField(max_length=1000)
