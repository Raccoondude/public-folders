from django.urls import path
from . import views

urlpatterns = [
    path('sub/', views.sub, name="sub"),
    path('submit/', views.submit, name='submit'),
    path('remove/', views.remove, name="destory"),
    path('removeOwO/', views.removeOwO, name='destoryOwO'),
    path('create/', views.create, name='create'),
    path('createUwU/', views.createUwU, name='createUwU'),
    path('delete/', views.delete, name="delete"),
    path('del/', views.dele, name='del'),
    path('post/<int:url_ID>', views.post, name='Post'),
    path('view/upload/<int:url_ID>', views.upload, name="upload"),
    path('view/<int:url_ID>', views.view, name='view'),
    path('', views.index, name='index'),
]
