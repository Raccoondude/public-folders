from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import *
from .forms import *
# Create your views here.


#index page

def index(request):
    folders = Folder.objects.all()
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')                    
    return render(request, 'index.html', {'folder_list':folders, 'ip':ip})

#folder view page

def view(request, url_ID):
    OwO = Folder.objects.get(folder_ID=url_ID)
    name = OwO.folder_name
    ID = OwO.folder_ID
    images = Image.objects.filter(image_folder=OwO)
    return render(request, 'view.html', {'image_list':images, 'OwO':OwO})
#upload page

def upload(request, url_ID):
    form = UploadForm()
    OwO = Folder.objects.get(folder_ID=url_ID)
    return render(request, 'upload.html', {'name':OwO.folder_name, 'ID':url_ID, 'form':form})

#upload handling page

def post(request, url_ID):
    if request.method == "POST":
        MyForm = UploadForm(request.POST, request.FILES)
        if MyForm.is_valid():
            File = request.FILES['File']
            x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
            if x_forwarded_for:
                ip = x_forwarded_for.split(',')[-1].strip()
            else:
                ip = request.META.get('REMOTE_ADDR')
            UwU = Folder.objects.get(folder_ID=url_ID)
            OwO = Image(image_image=File, image_folder=UwU, image_IP=ip)
            OwO.save()
            return HttpResponse("Thank You! <a href='/view/" + str(url_ID) + "'>Go Back</a>")
        
#mod delete post page
        
def delete(request):
    form = ModForm()
    return render(request, 'mod.html', {'form':form})

#delete post handling

def dele(request):
    if request.method == "POST":
        MyForm = ModForm(request.POST)
        if MyForm.is_valid():
            Username = MyForm.cleaned_data['Username']
            Password = MyForm.cleaned_data['Password']
            ID = MyForm.cleaned_data['ID']
            Note = MyForm.cleaned_data['Note']
            try:
                OwO = Mod.objects.get(mod_username=Username)
            except Mod.DoesNotExist:
                return HttpResponse("Invalid Username")
            if OwO.mod_password == Password:
                try:
                    UwU = Image.objects.get(image_ID=ID)
                except Image.DoesNotExist:
                    return HttpResponse("invalid ID")
                if OwO.mod_can_delete == True:
                    act = DelAct(act_post_ID=UwU.image_ID, act_username=Username, act_note=Note)
                    act.save()
                    UwU.delete()
                else:
                    return HttpResponse("You cannot delete")
                return HttpResponse("Done")
            else:
                return HttpResponse("invalid password")
            
#create folder page
            
def create(request):
    form = CreateForm()
    return render(request, 'create.html', {'form':form})

#folder create handling

def createUwU(request):
    if request.method == "POST":
        MyForm = CreateForm(request.POST)
        if MyForm.is_valid():
            username = MyForm.cleaned_data['Username']
            password = MyForm.cleaned_data['Password']
            name = MyForm.cleaned_data['Name']
            note = MyForm.cleaned_data['Note']
            try:
                OwO = Mod.objects.get(mod_username=username)
            except Mod.DoesNotExist:
                return HttpResponse("Invalid username")
            if password == OwO.mod_password:
                if OwO.mod_can_create == True:
                    UwU = Folder(folder_name=name)
                    UwU.save()
                    act = CreateAct(act_folder_ID=UwU.folder_ID, act_username=username, act_note=note)
                    return HttpResponseRedirect("/view/" + str(UwU.folder_ID))
                else:
                    return HttpResponse("You cannot do that")
            else:
                return HttpResponse("Invalid password")
            
#remove folder page
            
def remove(request):
    form = RemoveForm()
    return render(request, 'remove.html', {'form':form})

#remove folder handling

def removeOwO(request):
    if request.method == 'POST':
        MyForm = RemoveForm(request.POST)
        if MyForm.is_valid():
            username = MyForm.cleaned_data['Username']
            password = MyForm.cleaned_data['Password']
            ID = MyForm.cleaned_data['ID']
            note = MyForm.cleaned_data['Note']
            try:
                OwO = Mod.objects.get(mod_username=username)
            except Mod.DoesNotExist:
                return HttpResponse("invalid username")
            if OwO.mod_password == password:
                try:
                    UwU = Folder.objects.get(folder_ID=ID)
                except Folder.DoesNotExist:
                    return HttpResponse("invalid ID")
                if OwO.mod_can_remove == True:
                    UwU.delete()
                    act = RemoveAct(act_folder_ID=ID, act_username=username, act_note=note)
                    act.save()
                    return HttpResponse("Done")
            else:
                return HttpResponse("invalid password")

#mod application page
            
def submit(request):
    form = SubmitForm()
    return render(request, 'submit.html', {'form':form})

#mod application handling

def sub(request):
    if request.method == "POST":
        MyForm = SubmitForm(request.POST)
        if MyForm.is_valid():
            username = MyForm.cleaned_data['Username']
            password = MyForm.cleaned_data['Password']
            new_mod = Mod(mod_username=username, mod_password=password)
            new_mod.save()
            return HttpResponse("Thank you!")
